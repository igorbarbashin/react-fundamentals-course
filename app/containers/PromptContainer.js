import React, { Component, PropTypes } from 'react';
import Prompt from '../components/Prompt';

class PromptContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {username: props.username};
  }
  handleSubmitUser = (e) => {
    e.preventDefault();
    var username = this.state.username;
    this.setState({
      username: ''
    });

    if (this.props.routeParams.playerOne) {
      var pathname =
      this.context.router.push({
        pathname: '/battle',
        query: {
          playerOne: this.props.routeParams.playerOne,
          playerTwo: this.state.username,
        }
      })
    } else {
      this.context.router.push('/playerTwo/' + this.state.username)
    }
  };

  handleUpdateUser = (event) => {
    this.setState({
      username: event.target.value
    });
  };

  render() {
    return (
      <Prompt
        onSubmitUser={this.handleSubmitUser}
        onUpdateUser={this.handleUpdateUser}
        header={this.props.route.header}
        username={this.state.username} />
    )
  }
};

PromptContainer.contextTypes = {
  router: PropTypes.object.isRequired
};

PromptContainer.defaultProps = {
  username: ''
}

export default PromptContainer;