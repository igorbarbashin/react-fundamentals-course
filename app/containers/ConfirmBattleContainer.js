import React, {Component, PropTypes} from 'react';
import ConfirmBattle from '../components/ConfirmBattle';
import {getPlayersInfo} from '../utils/githubHelper'

class ConfirmBattleContainer extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            isLoading: true,
            playersInfo: []
        }
    }
    
    componentDidMount() {
        var query = this.props.location.query;
        getPlayersInfo([query.playerOne, query.playerTwo])
            .then((players) => {
                this.setState({
                    isLoading: false,
                    playersInfo: [players[0], players[1]]
                })
            })
        
    }
    render() {
        return (
            <ConfirmBattle
                isLoading={this.state.isLoading}
                playersInfo={this.state.playersInfo}
            />
        )
    }
}

ConfirmBattleContainer.contextTypes = {
  router: PropTypes.object.isRequired
};

ConfirmBattleContainer.PropTypes = {
    
}

export default ConfirmBattleContainer;
