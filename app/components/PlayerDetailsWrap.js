import React from 'react';

export default function PlayerDetailsWrap(props) {
    return (
        <div className="col-xs-6">
            {props.children}
        </div>
    )
}