import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import PlayerDetailsWrap from './PlayerDetailsWrap';
import PlayerDetails from './PlayerDetails';


function puke(obj) {
  return <pre>{JSON.stringify(obj, null, ' ')}</pre>
}

function ConfirmBattle(props) {
  return props.isLoading === true
    ? <p>Loading</p>
    : <div>CONFIRM BATTLE: {puke(props)}
        
        <div className="row">
          <PlayerDetailsWrap header="Player One">
            <PlayerDetails playerInfo={props.playersInfo[0]}></PlayerDetails>
          </PlayerDetailsWrap>
          <PlayerDetailsWrap header="Player Two">
            <PlayerDetails playerInfo={props.playersInfo[1]}></PlayerDetails>
          </PlayerDetailsWrap>
        </div>
        
        <Link to='/results'>
          <button className="btn-primary">
            Fight!
          </button>
        </Link>
        <Link to='/playerOne'>
          <button>Reselect players</button>
        </Link>
      </div>
}

ConfirmBattle.PropTypes = {
  isLoading: PropTypes.bool.isRequired,
  playersInfo: PropTypes.array.isRequired,
  onInitialBattle: PropTypes.func.isRequired
}

export default ConfirmBattle;