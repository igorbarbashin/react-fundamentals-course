import axios from 'axios';

let id = 'YOUR_CLIENT_ID';
let sec = 'YOUR_SECRET_ID';
let param = '?client_id=' + id + '&client_secret=' + sec;

function getUserInfo(username) {
    return axios.get('https://api.github.com/users/' + username + param);
}


export function getPlayersInfo(players) {
    return axios.all(players.map((player) => getUserInfo(player)))
    .then((info) => info.map((player) => player.data));
}

export let helpers = {
    getPlayersInfo
}